import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartingPageComponent } from './starting-page/starting-page.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { EndResultsComponent } from './end-results/end-results.component';

const routes: Routes = [
  { path: '', component: StartingPageComponent},
  { path: 'questionnaire/:testName', component: QuestionnaireComponent },
  { path: 'questionnaire/:testName/endResults', component: EndResultsComponent},
  { path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
