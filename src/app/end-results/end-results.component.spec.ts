import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndResultsComponent } from './end-results.component';

describe('EndResultsComponent', () => {
  let component: EndResultsComponent;
  let fixture: ComponentFixture<EndResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
