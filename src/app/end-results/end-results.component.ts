import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuizdataproviderService } from '../quizdataprovider.service';

@Component({
  selector: 'app-end-results',
  templateUrl: './end-results.component.html',
  styleUrls: ['./end-results.component.less']
})
export class EndResultsComponent implements OnInit, OnDestroy {

  private routeSub:Subscription;
  testName:string;
  displayName:Observable<string>;
  private results:boolean[];
  get totalNum():number {
    return this.results.length
  }
  get correctNum():number {
    return this.results.reduce((counter:number, res:boolean) => res ? ++counter : counter, 0)
  }
  get completed():boolean {
    return this.results && this.results.length > 0
  }

  @Input() successText: string;
  @Input() failText: string;

  constructor(private dataService:QuizdataproviderService, private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.params.subscribe(params => {
      this.testName = params["testName"];
      this.results = this.dataService.getResults(this.testName);
      this.displayName = this.dataService.getTestData(this.testName).pipe(map(data => data.testName))
    });
  }

  ngOnDestroy(): void {
    if(this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

}
