import { TestBed } from '@angular/core/testing';

import { QuestionproviderService } from './questionprovider.service';

describe('QuestionproviderService', () => {
  let service: QuestionproviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionproviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
