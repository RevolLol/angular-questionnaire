import { Component, OnInit, ViewChild, ElementRef, Output, OnDestroy, Input, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.less']
})
export class QuestionComponent implements OnInit, OnDestroy {

  @Input()
  text:string;
  @Input()
  options:string[];
  @Input()
  correctIndex:number[];
  @Input()
  infoCorrect:string;
  @Input()
  infoWrong:string;
  @Input()
  type:string;
  @Input()
  dataLink:string;

  @Output()
  next = new EventEmitter<boolean>();

  selectedOption:number;
  result:boolean;
  get isConfirmed() { return this.result != null }

  constructor() {

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes:SimpleChanges) {
    this.selectedOption = -1;
    this.result = null;
  }

  onNextQuestionClick(){
    if(this.result != null) {
      this.next.emit(this.result)
    }
    else {
      this.result = this.correctIndex.includes(this.selectedOption);
    }
  }

  ngOnDestroy() {
  }

}
