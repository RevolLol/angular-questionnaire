import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, share, first, tap } from 'rxjs/operators';

import { QuizdataproviderService } from '../quizdataprovider.service';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.less']
})
export class QuestionnaireComponent implements OnInit, OnDestroy {

  private routeSub: Subscription;
  private testName: string;
  private results: boolean[];
  private clickSub: Subscription;

  private testData:Observable<any>;

  get displayName():Observable<any> {
    return this.testData.pipe(map(data => data.testName))
  }

  qData:Observable<any>;

  private getQuestionData(index:number) {
    return this.testData.pipe(map(data => {
      return data.questions[index]
    }))
  }

  questionNumber: number;

  constructor(private dataService:QuizdataproviderService, private activatedRoute: ActivatedRoute, private router:Router) {

  }

  ngOnInit(): void {
    this.routeSub = this.activatedRoute.params.subscribe(params => {
      this.testName = params["testName"];
      this.testData = this.dataService.getTestData(this.testName).pipe(share());
      this.results = [];
      this.questionNumber = -1;
	  });
  }

  ngOnDestroy() {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
      this.routeSub = null;
    }
  }

  onTestStart() {
    this.questionNumber = 0;
    this.qData = this.getQuestionData(this.questionNumber);
  }

  onNextQuestion(result:boolean) {
    this.results.push(result);
    if(this.clickSub) {
      this.clickSub.unsubscribe();
    }
    this.clickSub = this.testData.pipe(first(), tap(data => {
      if(data.questions.length - 1 === this.questionNumber) {
        this.dataService.setResults(this.testName, this.results);
        this.router.navigate(["endResults"], {relativeTo: this.activatedRoute});
      }
      else {
        this.qData = this.getQuestionData(++this.questionNumber);
      }
    })).subscribe(() => {});
  }

}
