import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, merge } from 'rxjs';
import { map, shareReplay, reduce } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuizdataproviderService {

  private indexCache: Observable<any>;
  private testCache: { [key: string]: Observable<any> } = {};
  private results: { [key: string]: boolean[] } = {};

  constructor(private http: HttpClient) {
    this.getTestIndex(); 
  }

  getTestIndex(): Observable<any>{
    if(!this.indexCache) {
      this.indexCache = this.http.get<any>("assets/tests/index.json").pipe(shareReplay(1));
    }
    return this.indexCache
  }

  getTestData(testName:string) {
    if(!this.testCache[testName]) {
      this.testCache[testName] = this.http.get<any>(`assets/tests/data/${testName}.json`).pipe(shareReplay(1));
    }
    //
    /**
     * merging questions and test title into a single object
     * in order to combine results both data observables are mapped to return a callback, that accepts a resulting data object
     * then both mapped observables are merged, and the resulting observable is reduced to create final data object, that will be returned
     */
    const questionSet:Observable<any> = this.testCache[testName].pipe(map(questions => resData => Object.assign(resData, {questions})));
    const testNameSet:Observable<any> = this.getTestIndex().pipe(map(indexData => resData => Object.assign(resData, {testName: indexData[testName]})));
    return merge(questionSet, testNameSet).pipe(reduce((resData, fSetter) => fSetter(resData), {}));
  }

  getTestQuestion(testName:string, questionNum:number) {
    return this.getTestData(testName).pipe(map(data => data.questions[questionNum]))
  }

  getResults(testName:string):boolean[] {
    return this.results[testName]
  }

  setResults(testName:string, results:boolean[]) {
    this.results[testName] = results;
  }
}
