import { TestBed } from '@angular/core/testing';

import { QuizdataproviderService } from './quizdataprovider.service';

describe('QuizdataproviderService', () => {
  let service: QuizdataproviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuizdataproviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
