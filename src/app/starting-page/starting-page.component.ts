import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuizdataproviderService } from '../quizdataprovider.service';

@Component({
  selector: 'app-starting-page',
  templateUrl: './starting-page.component.html',
  styleUrls: ['./starting-page.component.less']
})
export class StartingPageComponent implements OnInit {

  @Input() welcomeText = "Привет, устраивайся поудобнее, и выбирай тест ниже";
  
  private testIndex: Observable<any>;
  get testIndexData(): Observable<any>{
    return this.testIndex
  }

  constructor(private dataService:QuizdataproviderService) { }

  //maps incoming object-like index into an array for convenience
  ngOnInit(): void {
    this.testIndex = this.dataService.getTestIndex().pipe(map(oIndex => { 
      return Object.keys(oIndex).map(sKey => ({ "name": sKey, "title": oIndex[sKey] })) 
    }));
  }

}
